# VNkline 

#### 介绍
Python专用的实时K线数据接口。

安装命令： pip install vnkline 计划做成PIP安装后直接运行，目前还是PyQt的框架 

 ![https://gitee.com/vnpypro/vnklineservice/raw/master/image/demo.png](https://gitee.com/vnpypro/vnklineservice/raw/master/image/demo.png)

#### 说明

从2023年12月7日开始，推出VNkline（实时K线数据）。

因为K线服务涉及数据流量成本和人工维护的成本，也是支持开源模式的一部分
为了保证服务质量，开展收费服务，具体可咨询https://www.vnpy.cn 官网客服
或通过关注微信公众号，向微信公众号提问。

CTP K线服务 免费，              0元/年，每分查询不超过1次，存在延迟、多用户争夺带宽的问题
获取IP咨询客服
 
 ![https://images.gitee.com/uploads/images/2021/1127/144239_33e658b5_1204097.png](https://images.gitee.com/uploads/images/2021/1127/144239_33e658b5_1204097.png)
 

主要支持CTP接口，支持国内149家期货公司程序化交易，实现程序化交易是免费的。
支持股指期货、商品期货、股指期权、商品期权，
支持中国8大合规交易所中的5所，包括上海期货交易所，大连期货交易所、
郑州期货交易所、中金所、能源所。

目录说明：
.idea：是Pycharm是项目配置文件目录，指定项目路径等等
strategy：  策略存放目录
temp： CTP接口产生的临时流文件存放目录


#### 软件架构
软件架构说明
这是使用PyQt调用 VNkline 调用K线数据并显示的例子。

#### Demo运行教程

1.  安装Python
2.  安装PyCharm
3.  安装PyQt, pyqtgraph，pandas, numpy 等模块


#### 使用说明

1.  接口文件只有3个文件vnklineservice.dll、vnklineservice.py、vnklineservice.ini
2.  其他属于对接口封装的框架，并用于显示K线
3.  VNPY开源软件和交易终端也使用了vnklineservice.dll服务


 

 ![https://images.gitee.com/uploads/images/2021/1127/144239_33e658b5_1204097.png](https://images.gitee.com/uploads/images/2021/1127/144239_33e658b5_1204097.png)
VNkline 
